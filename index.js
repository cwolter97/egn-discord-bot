const Discord = require("discord.js");
//const logger = require('winston');
//const botSettings = require("./settings.json");
var environment = 'remote';

var botSettings;

if(typeof process.env.token === 'undefined'){
  botSettings = require('./settings.json');
  environment = 'local';
}

var twitch_client_id, streamers, bot_token, yt_api_key, prefix, interval;

if(environment == 'local'){
  twitch_client_id = botSettings.twitch_api;
  bot_token = botSettings.token;
  prefix = botSettings.prefix;
  interval = botSettings.interval * 1000;
  streamers = botSettings.streamers;
  pubg_token = botSettings.pubg_token;
  pubg_players = botSettings.pubg_players;

  spam = true;
}else{
  twitch_client_id = process.env.twitch_api;
  bot_token = process.env.token;
  prefix = process.env.prefix;
  interval = process.env.interval * 1000;
  streamers = process.env.streamers;
  pubg_token = process.env.pubg_token;
  pubg_players = process.env.pubg_players;

  spam = false;
}

//Checking Online Streamer Status
const streamers_array = streamers.split(" ");
var streamer_status = new Array(streamers_array.length);
var streamer_list = {};

streamers_array.forEach(function(item){
    streamer_list[item] = false;
});

function checkStreamStatus(list){
  for(name in list){

    stream_name = name;
    isLive = list[name];

    console.log("checking stream status of: " + stream_name);
    //if(isLive == true){ console.log("already live"); }
    var request = require("request-promise");

    var options = { method: 'GET',
      url: `https://api.twitch.tv/kraken/streams/${stream_name}`,
      headers: {
        'Client-ID': twitch_client_id
      }
    }

    request(options, function(error, response, body){
      let jsonResponse = JSON.parse(body);

      //strim_name = stream_name;

      if(jsonResponse.stream != null){
        var rightNow = new Date();
        var x = rightNow.toISOString();
        //console.log(jsonResponse.stream);
        let embed = new Discord.RichEmbed()
            //.setAuthor(message.author.usernam)
            .setAuthor(jsonResponse.stream.channel.display_name, jsonResponse.stream.channel.logo)
            //.setDescription(jsonResponse.stream.channel.display_name + " is streaming: ")
            .setColor("#9B59B6")
            .setDescription("**Playing**: " + jsonResponse.stream.game)
            .setTitle(jsonResponse.stream.channel.status)
            .setURL(jsonResponse.stream.channel.url)
            .setImage(jsonResponse.stream.preview.medium)
            .setTimestamp(x)

            if(spam){
              let channel = channel_spam;
            } else {
              let channel = channel_strims;
            }

            if(jsonResponse.stream != null){ isLive = list[jsonResponse.stream.channel.name] };

            if(!isLive){
              channel_strims.send("Now Live: " + jsonResponse.stream.channel.display_name + "! ");//@here");
              channel_strims.send(embed);
              list[jsonResponse.stream.channel.name] = true;
              console.log(`${jsonResponse.stream.channel.name}.online = ${list[jsonResponse.stream.channel.name]}`);
              //console.log(`online: `);
            }
        } else {
          strim_name = jsonResponse._links.channel.substring(jsonResponse._links.channel.lastIndexOf('/')+1)
          streamer_list[strim_name] = false;
          console.log(`${strim_name}.online = ${list[strim_name]}`);
          //console.log(`online: ${list[strim_name]}`);
        }
    })
  }
}
//End Of Checking Streamer Status

//Checking For PUBG Chiken Diner
var lastCheck = new Date();

pubg_players_array = pubg_players.split(",");
pubg_status_check = new Array(pubg_players_array.length);

var pubg_lastCheck = {listExists: true};

pubg_players_array.forEach(function(item){
    pubg_lastCheck[item] = lastCheck;
});

delete pubg_lastCheck['listExists'];

function newMatch(updatedTime, player){
  console.log(updatedTime);
 	update = new Date(updatedTime);
	timediff = (update - pubg_lastCheck[player])/1e3/60;
	// console.log(`${timediff} minutes since last profile update`);
	// console.log(`Current Profile Update: ${update}`);
	// console.log(`Last Profile Update: ${lastCheck} (Could be time bot was started)`);

	if(timediff > 0){
		console.log(`last check updated for ${player}!`);
		pubg_lastCheck[player] = update;
		return true;
  } else {
    console.log(`no new matches for ${player}!`)
    return false;
  }
}

function postChickenDinner(){
  var request = require("request-promise");

  var options = { method: 'GET',
    url: `https://api.playbattlegrounds.com/shards/pc-na/players?filter[playerNames]=${pubg_players}`,
    headers: {
      'Authorization': `Bearer ${pubg_token}`,
      'Accept': 'application/vnd.api+json'
    }
  }

  request(options, function(error, response, body){
    console.log("Making Request");
  }).then((body) => {
    console.log("Request Complete");
    let jsonResponse = JSON.parse(body);
    //console.log(jsonResponse);
    jsonResponse.data.forEach((player) => {
      console.log("FROM REQUEST TIME: " + player.attributes.updatedAt);
      if(newMatch(player.attributes.updatedAt, player.attributes.name)){
          console.log("--------NEW MATCH---------");
          lastMatch = player.relationships.matches.data[0].id;
          console.log(`Last Match ID: ${lastMatch}`);

          var matchInfoRequest = require("request-promise");
          var options = { method: 'GET',
            url: `https://api.playbattlegrounds.com/shards/pc-na/matches/${lastMatch}`,
            headers: {
              'Authorization': `Bearer ${pubg_token}`,
              'Accept': 'application/vnd.api+json'
            }
          }
          matchInfoRequest(options, function(matchError, matchResponse, matchBody){
              console.log("Making Match Info Request for " + player.attributes.name);
          }).then((matchBody) => {
              console.log("Completed Match Info Request for " + player.attributes.name);
              console.log("-----MATCH END INFO------")
              matchResponse = JSON.parse(matchBody);
              //console.log(matchResponse);
              var x = 0, y = 0;
              winners = new Array();
              matchResponse.included.forEach(function(item){
              	//console.log(item.type)
              	if(item.type == "participant"){
                      if(item.attributes.stats.winPlace == 1){
              			       //console.log(`Winner of a Chicken Dinner: ${item.attributes.stats.name}`);
                           winners[y] = item.attributes.stats.name;
                           y++;
                      }
                      x++;
                  }
              });
              if(winners.includes(player.attributes.name)){
                pubg_msg = `${player.attributes.name} just earned a chiken diner!`;
                pubg_discord_msg = `\n\`\`\`DINNER NOTIFICATION!\n${player.attributes.name} just earned a chiken diner with ${item.attributes.stats.kills} kill(s)!\`\`\``
              } else {
                pubg_msg = `${player.attributes.name} lost to ${winners}!`
                //pubg_discord_msg = `\n\`\`\`DINNER NOTIFICATION!\n${player.attributes.name} just lost to ${winners}!\`\`\``;
                pubg_discord_msg = "";
              }
              console.log(pubg_msg);
              if(pubg_discord_msg == ""){ return; }
              if(spam){
                channel_spam.send(pubg_discord_msg);
              } else {
                channel_frags.send(pubg_discord_msg);
              }
          });
      }
    });

  }).catch(console.error);;
}
//End of Checking for Chiken Diner

//check for new announcements
function decode64(input) {
    var output = "";
    var hex = "";
    var chr1, chr2, chr3 = "";
    var enc1, enc2, enc3, enc4 = "";
    var i = 0;

    var b64array = "ABCDEFGHIJKLMNOP" +
           "QRSTUVWXYZabcdef" +
           "ghijklmnopqrstuv" +
           "wxyz0123456789+/" +
           "=";

    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
        console.log("invalid input");
		return;
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
        enc1 = b64array.indexOf(input.charAt(i++));
        enc2 = b64array.indexOf(input.charAt(i++));
        enc3 = b64array.indexOf(input.charAt(i++));
        enc4 = b64array.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);

	return output;
}

announcements_array = new Array();

function parseShit(json){

  for(property in json){
      json[property] = decode64(json[property])
      //console.log(json[property]);
      json[property] = json[property].replace(/(<([^>]+)>)/ig,"");
      json[property] = json[property].replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
          var num = parseInt(numStr, 10); // read num as normal number
          return String.fromCharCode(num);
      });
  }

  startOfSpoiler = json["POST_HTML"].indexOf("Spoiler: Hide'; ");

  //console.log(str);
  var str = json["POST_HTML"];

  while(startOfSpoiler > 0){ //while a spoiler exists
      //str = ;
      endOfSpoiler = (str.indexOf("Show\n", startOfSpoiler) + "Show\n".length );
      x = str.substring(startOfSpoiler, endOfSpoiler);
      str = str.replace(x, "");
      //console.log(x);

      startOfSpoiler = str.indexOf("Spoiler: Hide'");

      console.log("start: " + startOfSpoiler);
      console.log("end: " + endOfSpoiler);
  }

  json["POST_HTML"] = str;

  return json;
}

function getAnnouncements(){
  request = require("request-promise");
  options = { method: 'GET' , url: "http://elevatedgaming.net/forums/hpa_api.php" }
  return request(options, function(error, response, body){
      return body;
  })
}

function checkAnnouncements(){
  getAnnouncements().then((body) => {
      jsonResponse = JSON.parse(body);

      //announcements_array[] = 0;

      //announcements_array[2] = 1;

      var x=0,new_announcement//,new_announcements=new Array();

      for(announcement_id in jsonResponse){
          if(announcements_array.includes(announcement_id)){
            //console.log(`${announcement_id} already exists!`);
          }else{
            new_announcement = parseShit(jsonResponse[announcement_id]);
            console.log(`NEW ANNOUNCEMENT!`);
            console.log(`Current Announcement IDs: ${announcements_array}`);
            console.log(`New Announcement ID: ${announcement_id}`);
            announcements_array.push(announcement_id);
            break;
          }
      }

      if(new_announcement){ //post new announcement
        //console.log(new_announcement)

        new_announcement.TOPIC_LINK = new_announcement.TOPIC_LINK.substring(1); //removes the . from the url

        x = new Date(new_announcement.TOPIC_DATE)
        x = x.toISOString();

        if(new_announcement.POST_HTML.length > 2048){
            new_announcement.POST_HTML = new_announcement.POST_HTML.substring(0,2045) + "...";
        }

        egn_base_url = "http://elevatedgaming.net/forums"
        topic_url = decodeURIComponent(new_announcement.TOPIC_LINK).replace("&amp;","&");

        let embed = new Discord.RichEmbed()
            .setAuthor(new_announcement.TOPIC_AUTHOR)
            .setColor("#9B59B6")
            .setDescription(new_announcement.POST_HTML)
            .setTitle(new_announcement.TOPIC_TITLE)
            .setURL(`${egn_base_url}${topic_url}`)
            .setTimestamp(x)
        channel_announcements.send("@everyone - New Home Page Announcement!");
        channel_announcements.send(embed);
      }

  });
}

function  initializeAnnouncements(){
    getAnnouncements().then((body) => {
        jsonResponse = JSON.parse(body);

        var x=0;
        for(announcement_id in jsonResponse){ //Removes HTML encoding & HTML tags
            announcements_array[x] = announcement_id;
            x++;

        }
        //console.log(announcements_array);
    });
}
//end of announcements

var channel_commands, channel_announcements;

const bot = new Discord.Client({disableEveryone: false});

bot.on("ready", async () => {
    username = "Community Bot";

    //Channel names
    if(spam){
        channel_commands = bot.guilds.find("name","BOT TESTING").channels.find("name", "bot-commands");
        channel_announcements = bot.guilds.find("name","BOT TESTING").channels.find("name", "announcements");
    }else{
        channel_commands = bot.guilds.find("name","ElevatedGamingNetwork").channels.find("name", "bot-commands");
        channel_announcements = bot.guilds.find("name","ElevatedGamingNetwork").channels.find("name", "announcements");
    }

    bot.user.setUsername(username);
});

bot.on("message", async message => {

    //command validation
    if(message.author.bot) return;
    //if(message.channel.type === "dm") return;

    if(message.channel.id != channel_commands.id) return;

    let messageArray = message.content.split(" ");
    let command = messageArray[0];

    if(!command.startsWith(prefix) && command != `getprefix`) return;

    //console.log(command);

    if (message.member.hasPermission("BAN_MEMBERS", undefined, true, undefined)){
      // allowed
      admin = true;
    } else {
        // not allowed
      admin = false;
    }

    //commands available to everyone
    switch(command) {
        case `${prefix}help`:
            msg = "```\n";
            msg += `${prefix}help: this command. duh.\n`;
            msg += `getprefix: returns ${username}'s current prefix!\n`;
            //template for adding new help command
            //msg += `${prefix}\n`

            msg += `\n`;

            if(admin){
              //admin help Commands
              msg += `${prefix}setprefix [string]: sets ${username}'s current prefix!\n`;
            }

            msg += "```";

            message.channel.send(msg);
            // message.author.createDM().then((DM) => {
            //   DM.send(msg);
            // });
            break;
        case `${prefix}getprefix`:
        case `getprefix`:
            message.channel.send(`The prefix is: \`${prefix}\``);
            break;
    }

    if(!admin) return;
    //Admin Commands
    switch(command) {
        case `${prefix}setprefix`:
          if(messageArray.length != 2){
              message.channel.send(`\`${prefix}setprefix [prefix]: Sets ${username}'s current prefix!\n\``);
              break;
          }
          prefix = messageArray[1];
          message.channel.send(`The prefix is now: \`${prefix}\``);
          break;

    }
});


bot.login(bot_token).then((token) => {
    console.log("EgN Bot Successfully Logged In!");
    initializeAnnouncements();
    setInterval(() => {
        checkAnnouncements();
    }, 10000);
}).catch(console.error);
